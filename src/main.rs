mod get_change;

use get_change::Change;


fn main() {
    let change_amount: Change = Change::calculate(543);

    println!("{} Quarters", change_amount.quarters);
    println!("{} Dimes", change_amount.dimes);
    println!("{} Nickles", change_amount.nickles);
    println!("{} Pennies", change_amount.pennies);
}
