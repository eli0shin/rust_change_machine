pub struct Change {
  pub quarters: i64,
  pub dimes: i64,
  pub nickles: i64,
  pub pennies: i64,
}

impl Change {
  pub fn calculate (input: i64) -> Change {
    let mut dimes: i64 = 0;
    let mut nickles: i64 = 0;
    let mut pennies: i64 = 0;

    let quarters: i64 = if input > 24 {
      dimes = input % 25;
      ( input - dimes ) / 25
     } else { 0 };

    dimes = if dimes > 9 {
      nickles = dimes % 10;
      ( dimes - nickles ) / 10
    } else { 0 };

    nickles = if nickles > 4 {
      pennies = nickles % 5;
      ( nickles - pennies ) / 5
    } else { 0 };

    Change {
      quarters: quarters,
      dimes: dimes,
      nickles: nickles,
      pennies: pennies,
    }
  }
}